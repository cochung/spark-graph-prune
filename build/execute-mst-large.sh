#!/bin/sh
cd ..

if [[ $# -eq 0 ]] ; then
    echo 'you need to enter input'
    exit 1
fi



path=$(hdfs getconf -confKey fs.defaultFS)

hdfs dfs -rm -r -f $path/user/$USER/*
hdfs dfs -mkdir $path/user/$USER/input

export WORKDIR=`pwd`

var=1



echo "sconfig: large file"
# numV=1696415
# numE=11095298
#base3=3500000
fileName=$1
numV=$2
numE=$3
base3=$4

# echo $fileName
# echo $base3

hdfs dfs -put input/$fileName $path/user/$USER/input/$fileName

for((j=8;j<9;j=j+1))
  do

    echo "debuginfo: fileName:$fileName ,Base $base3"
    spark-submit \
     --master local[$j] \
     --driver-memory 14G \
     --executor-memory 14G \
    --class org.distributedgraphalgorithms.Main \
    target/disalgorithms-1.0.jar 4 $numV $numE $base3 $path/user/$USER/input/$fileName
    
    echo "variable: "$var
    var=$((var + 1))

done
echo "------------------------------"

