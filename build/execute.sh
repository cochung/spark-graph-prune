#!/bin/sh
cd ..

if [[ $# -eq 0 ]] ; then
    echo 'you need to enter input'
    exit 1
fi

# nodes=10
# cores=10
alg=$1

filename="test.txt"

path=$(hdfs getconf -confKey fs.defaultFS)
# hdfs dfs -rm -r -f /user/$USER/*
# hdfs dfs -mkdir /user/$USER/input
# hdfs dfs -put /home/cheuk/Documents/code/spark-graph/input/$filename /user/$USER/input

#  hdfs dfs -rm -r -f /user/$USER/*
# hdfs dfs -mkdir /user/$USER/input

hdfs dfs -rm -r -f $path/user/$USER/*
hdfs dfs -mkdir $path/user/$USER/input



var=1




numV=100000
numE=3162277

echo "sconfig:  all base case"
base2=(1000000 501187 316228 223782 199525 177829 158489)
# base2=(1000000 501187 316228 223782 199525 177829 158489)
len2=${#base2[@]}

for((j=1;j<8;j=j+6))
  do
  echo "sconfig:------------different base case with same core---------------"
  for(( i=0; i<${len2}; i++ ));
  do
    echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores assignd_cores: $j node $numV edges $numE base ${base2[$i]} "
    spark-submit \
     --master local[$j] \
     --driver-memory 5g \
     --executor-memory 5G \
    --class org.distributedgraphalgorithms.Main \
    target/disalgorithms-1.0.jar $alg $numV $numE ${base2[$i]} $path/user/$USER/input/input.$var 
    
    echo "variable: "$var
    var=$((var + 1))
    

   echo "******************************************"
  done
done
echo "------------------------------"


echo "sconfig: vary core"

base=(1000000 223782)
len1=${#base[@]}

for(( i=0; i<${len1}; i++ ));
do
 
  for((j=1;j<9;j++))
  do
    echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores assignd_cores: $j node $numV edges $numE base ${base[$i]} "
    spark-submit \
    --master local[$j] \
    --class org.distributedgraphalgorithms.Main \
    target/disalgorithms-1.0.jar $alg $numV $numE ${base[$i]} $path/user/$USER/input/input.$var 

    echo "variable: "$var
    var=$((var + 1))
    

    echo "******************************************"
  done

done

