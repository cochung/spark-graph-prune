#!/bin/bash
#SBATCH --job-name="graph-spark"
#SBATCH --output="graph-spark.%j.%N.out"
#SBATCH --partition=compute
#SBATCH --nodes=10
#SBATCH --ntasks-per-node=8
#SBATCH --export=ALL
#SBATCH -t 01:30:00

cd ..
### Environment setup for Hadoop and Spark
export MODULEPATH=/share/apps/compute/modulefiles/applications:$MODULEPATH
module load spark/2.1.0
export HADOOP_CONF_DIR=$HOME/mycluster.conf



### Sleep for prolog race condition
sleep 30s
myhadoop-configure.sh

### Start HDFS.  Starting YARN isn't necessary since Spark will be running in
### standalone mode on our cluster.
start-dfs.sh

### Load in the necessary Spark environment variables
source $HADOOP_CONF_DIR/spark/spark-env.sh


### Start the Spark masters and workers.  Do NOT use the start-all.sh provided
### by Spark, as they do not correctly honor $SPARK_CONF_DIR
myspark start

 
export WORKDIR=`pwd`
path=$(hdfs getconf -confKey fs.defaultFS)
echo $path

hdfs dfs -rm -r -f $path/user/$USER/*
hdfs dfs -mkdir $path/user/$USER/input

 

hdfs dfs -put  input/$file2 $path/user/$USER/input/$file2


export WORKDIR=`pwd`

var=1

 
numV=334000
numE=925999
base3=700000

# fileName=as-skitter-in.txt 
fileName=com-amazon-in.txt
hdfs dfs -put input/$fileName $path/user/$USER/input/$fileName

 

echo "debuginfo: fileName:$fileName ,Base $base3"
spark-submit \
--driver-memory 10G \
--executor-memory 10G \
--class org.distributedgraphalgorithms.Main \
target/disalgorithms-1.0.jar 4 $numV $numE $base3 $path/user/$USER/input/$fileName
    
echo "variable: "$var
var=$((var + 1))

echo "------------------------------"







   




### Shut down Spark and HDFS
myspark stop
stop-dfs.sh

### Clean up
myhadoop-cleanup.sh
