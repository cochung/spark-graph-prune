#!/bin/bash
#SBATCH --job-name="graph-spark"
#SBATCH --output="graph-spark.%j.%N.out"
#SBATCH --partition=compute
#SBATCH --nodes=6
#SBATCH --ntasks-per-node=8
#SBATCH --export=ALL
#SBATCH -t 01:30:00

cd ..
### Environment setup for Hadoop and Spark
export MODULEPATH=/share/apps/compute/modulefiles/applications:$MODULEPATH
module load spark/2.1.0
export HADOOP_CONF_DIR=$HOME/mycluster.conf
export WORKDIR=`pwd`


### Sleep for prolog race condition
sleep 30s
myhadoop-configure.sh

### Start HDFS.  Starting YARN isn't necessary since Spark will be running in
### standalone mode on our cluster.
start-dfs.sh

### Load in the necessary Spark environment variables
source $HADOOP_CONF_DIR/spark/spark-env.sh


### Start the Spark masters and workers.  Do NOT use the start-all.sh provided
### by Spark, as they do not correctly honor $SPARK_CONF_DIR
myspark start

nodes=6
cores=8

alg=1

path=$(hdfs getconf -confKey fs.defaultFS)
echo $path

hdfs dfs -rm -r -f $path/user/$USER/*
hdfs dfs -mkdir $path/user/$USER/input

numV=100000
numE=3162277





var=1


echo "----------------------"
echo "sconfig:  all base case"
base2=(1000000 501187 316228 223782 199525 177829 158489)
len2=${#base2[@]}

for((j=1;j<42;j=j+40))
  do


  for(( i=0; i<${len2}; i++ ));
  do
    echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores assignd_cores: $j node $numV edges $numE base ${base2[$i]} "
    spark-submit \
    --total-executor-cores $j \
    --class org.distributedgraphalgorithms.Main \
    target/disalgorithms-1.0.jar $alg $numV $numE ${base2[$i]} $path/user/$USER/input/input.$var 
    
    var=$((var + 1))
    echo "******************************************"
  done
done
echo "----------------------"




echo "sconfig: vary core"
base=(1000000 223782)
len1=${#base[@]}
for(( i=0; i<${len1}; i++ ));
do

  for((j=1;j<10;j++))
  do
    echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores assignd_cores: $j node $numV edges $numE base ${base[$i]} "
    spark-submit \
    --total-executor-cores $j \
    --class org.distributedgraphalgorithms.Main \
    target/disalgorithms-1.0.jar $alg $numV $numE ${base[$i]} $path/user/$USER/input/input.$var
    
    var=$((var + 1))
    echo "******************************************"
  done

  for((j=10;j<41;j=j+10))
  do
    echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores assignd_cores: $j node $numV edges $numE base ${base[$i]} "
    spark-submit \
    --total-executor-cores $j \
    --class org.distributedgraphalgorithms.Main \
    target/disalgorithms-1.0.jar $alg $numV $numE ${base[$i]} $path/user/$USER/input/input.$var
    
    var=$((var + 1))
    echo "******************************************"
  done
done





### Shut down Spark and HDFS
myspark stop
stop-dfs.sh

### Clean up
myhadoop-cleanup.sh
