package org.distributedgraphalgorithms;

import java.io.*;
import java.util.*;
import java.util.Objects;
import java.util.Properties;
import java.net.URL;

public class Config {
    static Properties prop;

    static {
        try {
            // String fileName = "conf/default.properties";
            // FileInputStream fs = new FileInputStream(fileName);
            // prop = new Properties();
            // prop.load(fs);
            String fileName = "/conf/default.properties";
            Config.prop = new Properties();
            InputStream inputStream = Config.class.getResourceAsStream(fileName);
            Config.prop.load(inputStream);

        } catch (Exception eta) {

            eta.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        String value = Config.prop.getProperty(key);
        return value;
    }
}