package org.distributedgraphalgorithms;

import org.apache.spark.Partitioner;
import java.util.Random;

class CustomPartitioner extends Partitioner {

	private int numParts;

	public CustomPartitioner(int i) {
		numParts = i;
	}

	@Override
	public int numPartitions() {
		return numParts;

	}

	@Override
	public int getPartition(Object key) {

		//partition based on the first character of the key...you can have your logic here !!
		// return ((String)key).charAt(0)%numParts;
		// int key_c = ((Integer) key);
		// return (key_c + 1) % numParts;

		long x = (Long) key;
		Integer p = Integer.valueOf(Config.getProperty("prime"));

		Random rand = new Random();
		long a = rand.nextInt(p - 1) + 1;
		long b = rand.nextInt(p - 1) + 1;
		long tmpS = ((a * x + b) % p);
		long u = tmpS % numParts;
		int v = (int) u;

		return v;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CustomPartitioner) {
			CustomPartitioner partitionerObject = (CustomPartitioner) obj;
			if (partitionerObject.numParts == this.numParts)
				return true;
		}

		return false;
	}
}