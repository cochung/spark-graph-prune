package org.distributedgraphalgorithms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.storage.StorageLevel;
import static com.google.common.base.Preconditions.checkArgument;

public class DistributedMST {

    private List<Edge> mstList;
    private int numV;
    private long numE;
    private int base;

    public List<Edge> getMstList() {
        return this.mstList;
    }

    public DistributedMST(JavaRDD<String> readlines, int numV, long numE, int base) {
        this.numV = numV;
        this.numE = numE;
        this.base = base;
        JavaPairRDD<Long, Edge> edgePair = mapToEdgePair(readlines);
        edgePair.persist(StorageLevel.MEMORY_ONLY());
        this.mstList = getMergedMST(edgePair);

    }

    private JavaPairRDD<Long, Edge> mapToEdgePair(JavaRDD<String> readlines) {
        JavaPairRDD<Long, Edge> edgePair = readlines.zipWithIndex().mapToPair(t -> {
            long num = t._2;
            String[] edge = t._1.split(" ");
            int u = Integer.valueOf(edge[0]);
            int v = Integer.valueOf(edge[1]);
            double weight = Double.valueOf(edge[2]);
            Edge tmpEdge = new Edge(num, u, v, weight);

            return new Tuple2<Long, Edge>(num, tmpEdge);

        });

        return edgePair;
    }

    private JavaPairRDD<Long, Edge> getMSTPerPartition(JavaPairRDD<Long, Edge> edgePair) {

        JavaPairRDD<Long, Edge> mapPartIndexRDDs = edgePair.mapPartitionsToPair((v) -> {
            List<Edge> edges = new ArrayList<Edge>();
            while (v.hasNext()) {
                Tuple2<Long, Edge> tmp = v.next();
                edges.add(tmp._2);
            }

            Kruskal kruskal = new Kruskal(edges);
            List<Edge> mst = kruskal.getMST();
            List<Tuple2<Long, Edge>> ret = new ArrayList<Tuple2<Long, Edge>>();

            mst.forEach(e -> ret.add(new Tuple2<Long, Edge>(e.num, e)));

            return ret.iterator();
        }, false);

        return mapPartIndexRDDs;

    }

    private List<Edge> getMergedMST(JavaPairRDD<Long, Edge> edgePair) {
        int k = 0;
        while (true) {
            long num = edgePair.count();

            System.out.println("count:" + num);
            if (num <= base) {
                List<Edge> colEdges = new ArrayList<Edge>();
                edgePair.collect().forEach(t -> colEdges.add(t._2));
                Kruskal kruskal = new Kruskal(colEdges);
                List<Edge> mst = kruskal.getMST();
                System.out.println("round:  " + k);
                return mst;

            } else {
                k++;
                System.out.println("start partition");
                long pnum = num / (long) base + 1;
                int pnumint = (int) pnum;

                edgePair = edgePair.partitionBy(new CustomPartitioner(pnumint));
                edgePair = getMSTPerPartition(edgePair);
                edgePair.persist(StorageLevel.MEMORY_ONLY());
            }
        }

    }

}
