package org.distributedgraphalgorithms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.apache.spark.storage.StorageLevel;

public class DistributedMaximalMatching {
    private static final Logger LOGGER = LoggerFactory.getLogger(DistributedMST.class);
    private JavaRDD<String> readlines;
    private List<Edge> matchedEdges;
    private HashSet<Integer> matchedVertex;
    private HashSet<Long> matchedEdgeIndex;
    private int numV;
    private long numE;
    private int base;

    public DistributedMaximalMatching(JavaRDD<String> readlines, int numV, long numE, int base) {
        this.readlines = readlines;
        this.numV = numV;
        this.numE = numE;
        this.base = base;
        matchedEdges = new ArrayList<Edge>();
        matchedVertex = new HashSet<Integer>();
        matchedEdgeIndex = new HashSet<Long>();
        JavaPairRDD<Long, Edge> edgePair = mapToEdgePair(this.readlines);
        edgePair.persist(StorageLevel.MEMORY_ONLY());
        getMaximalMatching(edgePair);

    }

    public List<Edge> getMatchedEdges() {
        return matchedEdges;
    }

    public HashSet<Integer> getMatchedVertex() {
        return matchedVertex;
    }

    private JavaPairRDD<Long, Edge> mapToEdgePair(JavaRDD<String> readlines) {
        JavaPairRDD<Long, Edge> edgePair = readlines.zipWithIndex().mapToPair(t -> {
            long num = t._2;
            String[] edge = t._1.split(" ");
            int u = Integer.valueOf(edge[0]);
            int v = Integer.valueOf(edge[1]);
            int weight = -1;
            Edge tmpEdge = new Edge(num, u, v, weight);

            return new Tuple2<Long, Edge>(num, tmpEdge);

        });

        return edgePair;
    }

    private void getMaximalMatching(JavaPairRDD<Long, Edge> edgePair) {
        Boolean isLastRound = false;
        int k = 0;
        while (true) {

            long count = edgePair.count();
            System.out.println("count: " + count);
            List<Edge> colEdges = new ArrayList<Edge>();
            if (count <= base) {
                isLastRound = true;
                edgePair.collect().forEach(t -> colEdges.add(t._2));

            } else {
                k++;
                float p = (float) base / (10 * count);
                p = Math.min(p, 1.0f);
                System.out.println("probability:    " + p);
                JavaPairRDD<Long, Edge> sampleEdgePair = edgePair.sample(false, p);
                sampleEdgePair.collect().forEach(t -> colEdges.add(t._2));

                System.out.println("sampledgeCount: " + colEdges.size());
            }

            MaximalMatching maximal = new MaximalMatching(colEdges);
            List<Edge> edges = maximal.getMatchedEdge();
            HashSet<Integer> vertexList = maximal.getMatchedVertex();
            HashSet<Long> matchedEdgeIndex = maximal.getMatchedEdgeIndex();

            // ArrayList<Integer> vertexList2 = new ArrayList<Integer>();
            // vertexList.forEach(t -> vertexList2.add(t));

            mergedMatched(edges, vertexList, matchedEdgeIndex);
            if (isLastRound) {
                break;
            } else {
                System.out.println("filter");
                edgePair = edgePair.filter(t -> !(vertexList.contains(t._2.u) || vertexList.contains(t._2.v)));
                edgePair.persist(StorageLevel.MEMORY_ONLY());

            }

        }
        System.out.println("round:  " + k);
    }

    private void mergedMatched(List<Edge> edges, HashSet<Integer> vertexList, HashSet<Long> matchedEdgeIndex) {
        this.matchedEdges.addAll(edges);
        this.matchedVertex.addAll(vertexList);
        this.matchedEdgeIndex.addAll(matchedEdgeIndex);
    }

}
