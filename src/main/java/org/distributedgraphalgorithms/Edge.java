package org.distributedgraphalgorithms;

import java.io.Serializable;

public class Edge implements Serializable, Comparable<Edge> {
    long num;
    int u;
    int v;
    double weight;
    int partition = -1;//debug purpose

    Edge(long num, int u, int v, double weight) {
        this.num = num;
        this.u = u;
        this.v = v;
        this.weight = weight;
    }

    Edge(int partition, Edge e) {
        this.num = e.num;
        this.u = e.u;
        this.v = e.v;
        this.weight = e.weight;
        this.partition = partition;
    }

    @Override
    public String toString() {
        // String out = "num " + num + " u " + u + " v " + v + " weight " + weight + " partition " + partition;
        String out = u + " " + v + " " + String.valueOf(weight);
        return out;
    }

    @Override
    public int compareTo(Edge e2) {
        Edge e1 = this;

        double diff = e1.weight - e2.weight;
        double c = 0.00001f;

        if (Math.abs(diff) < c)
            return 0;
        else if (diff > 0)
            return 1;
        else
            return -1;

    }

}