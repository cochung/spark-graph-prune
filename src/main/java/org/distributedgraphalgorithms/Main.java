package org.distributedgraphalgorithms;

import org.apache.spark.SparkConf;

import org.apache.spark.api.java.JavaSparkContext;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;
import org.apache.spark.storage.StorageLevel;
import java.util.Random;
import java.util.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        System.out.print("parameters:   ");
        for (String str : args) {
            System.out.print(str + " ");
        }
        System.out.println();
        int src = Integer.valueOf(args[0]);
        int numV = Integer.valueOf(args[1]);
        long numE = Long.valueOf(args[2]);
        int base = Integer.valueOf(args[3]);
        String fileName = args[4];
        switch (src) {
        case 1:
            runMST(numV, numE, base, fileName);
            break;
        case 2:
            runMaximalMatching(numV, numE, base, fileName);
            break;
        case 3:
            runConnectedComponent(numV, numE, base, fileName);
            break;
        case 4:
            runMSTwithLarge(numV, numE, base, fileName);
        // case 4:
        // init(numV, numE, base, fileName);
        // case 5:
        // runcc(numV, numE, base, fileName);
        // test_();
        default:
            break;
        }

    }

    public static void test_() {
        SparkConf conf = new SparkConf().setAppName("MaximalMatching");

        for (Tuple2<String, String> t : conf.getAll()) {
            System.out.println(t);
        }

        scala.collection.JavaConversions.seqAsJavaList(conf.getExecutorEnv()).forEach(System.out::println);
        JavaSparkContext sc = new JavaSparkContext(conf);

        int size1 = 1000000;
        int size2 = 50000;
        List<Integer> list1 = new ArrayList<Integer>();
        for (int i = 0; i < size1; i++) {
            list1.add(i);
        }
        List<Integer> list2 = new ArrayList<Integer>();
        for (int i = 0; i < size2; i++) {
            list2.add(i);
        }

        JavaRDD<Integer> rdd = sc.parallelize(list1);
        System.out.println(rdd.count());
        rdd.persist(StorageLevel.MEMORY_ONLY());
        JavaRDD<Integer> rdd2 = rdd.filter(t -> list2.contains(t));
        long start = System.currentTimeMillis();
        System.out.println(rdd2.count());
        long end = System.currentTimeMillis();
        ElapsedTime.logElapsedTime(start, end, "Maximalmatching");

    }

    public static void init(int numV, long numE, int base, String fileName) {
        SparkConf conf = new SparkConf().setAppName("MaximalMatching");
        JavaSparkContext sc = new JavaSparkContext(conf);
        Test.initMSTTestData(sc, numV, numE, base, fileName);
    }

    public static void runMaximalMatching(int numV, long numE, int base, String fileName) {

        SparkConf conf = new SparkConf().setAppName("MaximalMatching");

        for (Tuple2<String, String> t : conf.getAll()) {
            if (t._1.contains("cores"))
                System.out.println("sconfig:  " + t);
        }

        scala.collection.JavaConversions.seqAsJavaList(conf.getExecutorEnv()).forEach(System.out::println);
        JavaSparkContext sc = new JavaSparkContext(conf);
        // System.out.println("paral: " + sc.defaultParallelism());

        Test.initMaximalTestData(sc, numV, numE, base, fileName);

        long start = System.currentTimeMillis();

        JavaRDD<String> readlines = sc.textFile(fileName);

        DistributedMaximalMatching mm = new DistributedMaximalMatching(readlines, numV, numE, base);
        List<Edge> matchedEdges = mm.getMatchedEdges();
        HashSet<Integer> matchedVertex = mm.getMatchedVertex();
        System.out.println("matchedEdgeSize " + matchedEdges.size());
        System.out.println("matchedVertexSize " + matchedVertex.size());
        // System.out.println("matchedVertex: " + matchedVertex);

        long end = System.currentTimeMillis();
        ElapsedTime.logElapsedTime(start, end, "Maximalmatching");
    }

    public static void runMSTwithLarge(int numV, long numE, int base, String fileName) {

        // SparkSession spark = SparkSession.builder().appName("minimum spanning
        // tree").getOrCreate();
        // JavaSparkContext sc = new JavaSparkContext(spark.sparkContext());
        SparkConf conf = new SparkConf().setAppName("MST");
        JavaSparkContext sc = new JavaSparkContext(conf);

        for (Tuple2<String, String> t : conf.getAll()) {
            if (t._1.contains("cores"))
                System.out.println("sconfig:  " + t);
        }
        scala.collection.JavaConversions.seqAsJavaList(conf.getExecutorEnv()).forEach(System.out::println);


        long start = System.currentTimeMillis();

        // String readpath = Config.getProperty("msthdfspath") + "/part-*";
        // JavaRDD<String> readlines = sc.textFile(fileName + "/part-*");
        JavaRDD<String> readlines = sc.textFile(fileName);
        DistributedMST disMST = new DistributedMST(readlines, numV, numE, base);
        List<Edge> mstList = disMST.getMstList();
        double cost = 0;
        System.out.println("mstList.size:   " + mstList.size());
        // mstList.forEach(System.out::println);
        for (Edge e : mstList) {
            cost = cost + e.weight;
        }
        System.out.println("total_weight:" + String.format("%11.4f", cost));

        long end = System.currentTimeMillis();
        ElapsedTime.logElapsedTime(start, end, "MST");
    }


    public static void runMST(int numV, long numE, int base, String fileName) {

        // SparkSession spark = SparkSession.builder().appName("minimum spanning
        // tree").getOrCreate();
        // JavaSparkContext sc = new JavaSparkContext(spark.sparkContext());
        SparkConf conf = new SparkConf().setAppName("MST");
        JavaSparkContext sc = new JavaSparkContext(conf);

        for (Tuple2<String, String> t : conf.getAll()) {
            if (t._1.contains("cores"))
                System.out.println("sconfig:  " + t);
        }
        scala.collection.JavaConversions.seqAsJavaList(conf.getExecutorEnv()).forEach(System.out::println);

        Test.initMSTTestData(sc, numV, numE, base, fileName);

        long start = System.currentTimeMillis();

        // String readpath = Config.getProperty("msthdfspath") + "/part-*";
        // JavaRDD<String> readlines = sc.textFile(fileName + "/part-*");
        JavaRDD<String> readlines = sc.textFile(fileName);
        DistributedMST disMST = new DistributedMST(readlines, numV, numE, base);
        List<Edge> mstList = disMST.getMstList();
        double cost = 0;
        System.out.println("mstList.size:   " + mstList.size());
        // mstList.forEach(System.out::println);
        for (Edge e : mstList) {
            cost = cost + e.weight;
        }
        System.out.println("total_weight:" + String.format("%11.4f", cost));

        long end = System.currentTimeMillis();
        ElapsedTime.logElapsedTime(start, end, "MST");
    }

    public static void runConnectedComponent(int numV, long numE, int base, String fileName) {

        // SparkSession spark =
        // SparkSession.builder().appName("ConnectedComponent").getOrCreate();
        // JavaSparkContext sc = new JavaSparkContext(spark.sparkContext());

        SparkConf conf = new SparkConf().setAppName("ConnectComponent");
        JavaSparkContext sc = new JavaSparkContext(conf);

        for (Tuple2<String, String> t : conf.getAll()) {
            if (t._1.contains("cores"))
                System.out.println("sconfig:  " + t);
        }
        scala.collection.JavaConversions.seqAsJavaList(conf.getExecutorEnv()).forEach(System.out::println);

        Test.initMSTTestData(sc, numV, numE, base, fileName);

        long start = System.currentTimeMillis();
        // String readpath = Config.getProperty("msthdfspath") + "/part-*";
        JavaRDD<String> readlines = sc.textFile(fileName + "/part-*");

        DistributedMST disMST = new DistributedMST(readlines, numV, numE, base);

        List<Edge> mstList = disMST.getMstList();
        System.out.println(mstList.size());

        UnionFindSet ufset = new UnionFindSet();
        HashMap<Integer, ArrayList<Integer>> ccmap = ufset.getConnectedComponent(mstList);
        // System.out.println(ccmap.size());
        // ccmap.keySet().forEach(System.out::println);

        long end = System.currentTimeMillis();
        ElapsedTime.logElapsedTime(start, end, "ConnectedComonent");
    }

    public static void runcc(int numV, long numE, int base, String fileName) {
        long start = System.currentTimeMillis();
        List<Edge> col = new ArrayList<Edge>();
        long count = 0;

        Random rand = new Random();
        while (count < numE) {
            int a = rand.nextInt(numV - 1) + 1;
            int b = rand.nextInt(numV - 1) + 1;
            int c = rand.nextInt(numV - 1) + 1;
            if (a != b) {
                count++;
                Edge e = new Edge(-1, a, b, c);
                col.add(e);
            }
        }
        UnionFindSet ufset = new UnionFindSet();
        HashMap<Integer, ArrayList<Integer>> ccmap = ufset.getConnectedComponent(col);
        long end = System.currentTimeMillis();
        ElapsedTime.logElapsedTime(start, end, "ConnectedComonent");
    }

}

