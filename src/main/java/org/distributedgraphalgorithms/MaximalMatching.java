package org.distributedgraphalgorithms;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

public class MaximalMatching {
    private HashSet<Integer> matchedVertex;
    private HashSet<Long> matchedEdgeIndex;
    private List<Edge> matchedEdge;

    MaximalMatching(List<Edge> edgeList) {

        this.matchedVertex = new HashSet<Integer>();
        this.matchedEdgeIndex = new HashSet<Long>();
        this.matchedEdge = new ArrayList<Edge>();
        edgeList.forEach(e -> {
            boolean c = !matchedVertex.contains(e.u) && !matchedVertex.contains(e.v);
            if (c) {
                matchedVertex.add(e.u);
                matchedVertex.add(e.v);
                matchedEdge.add(e);
                matchedEdgeIndex.add(e.num);
            }

        });
    }

    public HashSet<Integer> getMatchedVertex() {
        return this.matchedVertex;
    }

    public HashSet<Long> getMatchedEdgeIndex() {
        return this.matchedEdgeIndex;
    }

    public List<Edge> getMatchedEdge() {
        return this.matchedEdge;
    }
}