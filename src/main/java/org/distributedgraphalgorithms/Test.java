package org.distributedgraphalgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.spark.api.java.JavaSparkContext;

public class Test {

    public static void initMSTTestData(JavaSparkContext sc, int numV, long numE, int base, String fileName) {
        try {

            List<Edge> col = new ArrayList<Edge>();
            long count = 0;

            Random rand = new Random();
            while (count < numE) {
                int a = rand.nextInt(numV - 1) + 1;
                int b = rand.nextInt(numV - 1) + 1;
                float c = rand.nextFloat() * 100000;
                if (a != b) {
                    count++;
                    Edge e = new Edge(-1, a, b, c);
                    // System.out.println(e);
                    col.add(e);
                }
            }

            sc.parallelize(col).saveAsTextFile(fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void initMaximalTestData(JavaSparkContext sc, int numV, long numE, int base, String fileName) {

        try {

            long start = System.currentTimeMillis();

            List<Edge> col = new ArrayList<Edge>();
            long count = 0;

            Random rand = new Random();
            while (count < numE) {
                int a = rand.nextInt(numV - 1) + 1;
                int b = rand.nextInt(numV - 1) + 1;
                int c = -1;
                if (a != b) {
                    count++;
                    Edge e = new Edge(-1, a, b, c);
                    col.add(e);
                }
            }

            long end = System.currentTimeMillis();
            // ElapsedTime.logElapsedTime(start, end, "testInit");

            long start2 = System.currentTimeMillis();
            sc.parallelize(col).saveAsTextFile(fileName);
            long end2 = System.currentTimeMillis();
            // ElapsedTime.logElapsedTime(start2, end2, "testWrite");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
