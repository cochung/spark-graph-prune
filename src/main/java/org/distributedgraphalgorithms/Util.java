package org.distributedgraphalgorithms;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;
import java.util.LinkedList;
public class Util {

    public static void printPerPartition(JavaPairRDD<Integer, Edge> edgePair) {
        JavaRDD<String> mapPartIndexRDDs = edgePair.mapPartitionsWithIndex((v1, v2) -> {
            LinkedList<String> linkedList = new LinkedList<String>();
            while (v2.hasNext()) {
                Tuple2<Integer, Edge> tmp = v2.next();
                linkedList.add(Integer.toString(v1) + "|" + tmp._1 + " ~ " + tmp._2.toString());
            }
            return linkedList.iterator();
        }, false);

        mapPartIndexRDDs.collect().forEach(v -> System.out.println(v));
        System.out.println();
    }
}